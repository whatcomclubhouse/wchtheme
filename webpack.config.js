const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");

const entry = {
	index: [
		"./src/typescript/index.ts"
	]
};

const export_module = {
	rules: [{
		test: /\.ts$/
		,use: "ts-loader"
	}]
};

const resolve = {
	extensions: [".ts", ".js", ".scss"]
};

const output_filename = "assets/js/[name].min.js";

const output_library = "wchtheme";

const plugins = [
	new CopyPlugin({
		patterns: [
			{
                from: "src/*.php"
				,to({ context, absoluteFilename }) {
                    return "[name][ext]";
                }
			},{
                from: "src/*.css"
				,to({ context, absoluteFilename }) {
                    return "[name][ext]";
                }
			},{
                from: "src/screenshot.png"
				,to({ context, absoluteFilename }) {
                    return "[name][ext]";
                }
			},{
                from: "src/manifest.json"
				,to: "assets/manifest.json"
			},{
                from: "COPYING"
				,to: "[name][ext]"
			}
		]
	})
]

const dist = {
	entry: entry
	,name: "dist"
	,mode: "production"
	,module: export_module
	,resolve: resolve
	,output: {
		filename: output_filename
		,path: path.resolve(__dirname, "dist")
		,library: output_library
	}
	,plugins: plugins
};

const dev = {
	entry: entry
	,name: "dev"
	,mode: "development"
	,devtool: "source-map"
	,devServer: {
		static: ["dev"],
		devMiddleware: { writeToDisk: true },
		proxy: { "/": { target: "http://localhost:80/" } }
	}
	,module: export_module
	,resolve: resolve
	,output: {
		filename: output_filename
		,path: path.resolve(__dirname, "dev")
		,library: output_library
	}
	,plugins: plugins
}

module.exports = [ dist, dev ];
