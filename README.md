# Whatcom Clubhouse WordPress Theme
This is a simple theme for the Whatcom Clubhouse WordPress website.

## IDE
This project is currently maintained using VSCode, and IDE support files are
provided.  The PHP Intellisense extension is encouraged, as is placing a copy
of the currently targeted version of wp-includes at the root.  These two tools
in tandem allow for relatively effective intellisense use.

However, consider ensuring "require_once" entries are present for all consumed
files relative to ABSPATH for wp-includes, this has shown to work for
intellisense in NetBeans and perhaps would support other PHP-aware IDEs as long
as ABSPATH can be made to point where a wp-includes folder is located.

It is unknown whether any intellisense can navigate require_once or other file
linking directives that are dependent on a runtime function (e.g. for WordPress
get_template_directory() comes to mind).  

Currently there is a bug with the browser debug linkage in that the
source mappings do not seem to work correctly.  Typescript mappings somehow
point to what looks like an intermediate file, and Sass mappings fail to load
outright.  Debug through the IDE appears to work, unknown if this would impact
other development environments.

## Restore
    npm install

## Build
    npm run build

## Debug
    npm run debug