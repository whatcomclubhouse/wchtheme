<?php 
/**
 * index - the general page template
 * 
 * Copyright 2022 Whatcom Clubhouse
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
require_once ABSPATH."/wp-includes/author-template.php";
require_once ABSPATH."/wp-includes/general-template.php";
require_once ABSPATH."/wp-includes/post-template.php";
require_once ABSPATH."/wp-includes/option.php";
require_once ABSPATH."/wp-includes/query.php";

get_header(); 

while (have_posts()) {
    the_post();
?>
    <div <?php post_class("my-2"); ?>>
        <h2><?php the_title(); ?></h2>
<?php
        if (get_post_type() == "post") {
?>
            <h6>At: <?php the_time(get_option("date_format")); ?></h6>
<?php
        }
?>
        <p><?php the_content(); ?></p>
    </div>
<?php
}

get_footer();
