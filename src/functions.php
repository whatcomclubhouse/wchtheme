<?php
/**
 * functions - theme functions and definitions
 * 
 * Copyright 2022 Whatcom Clubhouse
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
require_once ABSPATH."/wp-includes/l10n.php";
require_once ABSPATH."/wp-includes/plugin.php";
require_once ABSPATH."/wp-includes/post.php";
require_once ABSPATH."/wp-includes/theme.php";
require_once ABSPATH."/wp-includes/widgets.php";

require_once ABSPATH."/wp-includes/functions.wp-scripts.php";
require_once ABSPATH."/wp-includes/functions.wp-styles.php";

/**
 * @var string $wchtheme_version the theme version
 */
$wchtheme_version = "0.9.1";

/**
 * @var string $wchtheme_text_domain the text domain
 */
$wchtheme_text_domain = "wchtheme";

if (!function_exists("wchtheme_get_manifest")) {
    /**
     * Gets the path to the manifest.json
     * 
     * @return string the manifest path
     */
    function wchtheme_get_manifest(): string
    {
        return get_template_directory_uri()."/assets/manifest.json"; 
    }
}

if (!function_exists("wchtheme_support")) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * @return void
     */
    function wchtheme_support(): void
    {
        add_editor_style("style.css");
    }
}

if (!function_exists("wchtheme_styles")) {
    /**
     * Enqueue styles.
     *
     * @return void
     */
    function wchtheme_styles(): void
    {
        global $wchtheme_version;

        wp_register_style("wchtheme-style", get_template_directory_uri()."/style.css", array(), $wchtheme_version);
        wp_enqueue_style("wchtheme-style");
    }
}

if (!function_exists("wchtheme_scripts")) {
    /**
     * Enqueue scripts.
     *
     * @return void
     */
    function wchtheme_scripts(): void
    {
        global $wchtheme_version;

        wp_register_script("wchtheme-script", get_template_directory_uri()."/assets/js/index.min.js", array(), $wchtheme_version, true);
        wp_enqueue_script("wchtheme-script");
    }
}

if (!function_exists("wchtheme_widgets_init")) {
    /**
     * Initialize any widgets
     * 
     * @return void
     */
    function wchtheme_widgets_init(): void
    {
        global $wchtheme_text_domain;

        register_sidebar(array(
            "name"          => __("Sidebar", $wchtheme_text_domain),
            "id"            => "sidebar"
        ));

        register_sidebar(array(
            "name"          => __("Footer", $wchtheme_text_domain),
            "id"            => "footer",
            "before_widget" => "<footer class='fixed-bottom text-center border-top bg-light pt-2' id='wchtheme-footer'>",
            "after_widget"  => "</footer>"
        ));

        register_sidebar(array(
            "name"          => __("Front Page Cards", $wchtheme_text_domain),
            "id"            => "front_page_cards",
            "before_widget" => "<div class='card my-2'><div class='card-body'><p class='card-text'>",
            "after_widget"  => "</p></div></div>"
        ));
    }
}

if (!function_exists("wchtheme_get_carousel_attachments")) {
    /**
     * This function returns all attachment IDs of carousel images
     * Current implementation looks for "carousel_*" in the alt image tag
     * 
     * @return array
     */
    function wchtheme_get_carousel_attachments(): array
    {
        $attachments = get_posts(array(
            "post_type"     => "attachment",
            "meta_query"    => array(array(
                "key"           => "_wp_attachment_image_alt",
                "value"         => "^carousel_.*$",
                "compare"       => "REGEXP"
            ))
        ));

        return $attachments;
    }
}

if (!function_exists("wchtheme_get_regular_posts")) {
    /**
     * Returns a selection of regular posts
     * 
     * @return array an array of WP_Post objects
     */
    function wchtheme_get_regular_posts(): array
    {
        return get_posts(array(
            "post_type" => "post"
        ));
    }
}

if (!function_exists("wchtheme_has_regular_posts")) {
    /**
     * Determines if the current blog has "post"-type posts
     * 
     * @return bool
     */
    function wchtheme_has_regular_posts(): bool
    {
        $posts = wchtheme_get_regular_posts();

        return count($posts) != 0;
    }
}

if (!function_exists("wchtheme_nav_pages_sort")) {
    /**
     * Sort the navbar pages front page, then posts page,
     * then the rest of the collection
     * 
     * @param array $pages The collection of pages to sort
     * @return array
     */
    function wchtheme_nav_pages_sort(array $pages): array
    {
        $front_index = -1;
        $home_index = -1;
        $page_on_front = get_option("page_on_front");
        $page_for_posts = get_option("page_for_posts");
        $sorted = array();

        foreach ($pages as $index => $page) {
            switch ($page->ID) {
                case $page_on_front:
                    $front_index = $index;
                    break;
                case $page_for_posts:
                    $home_index = $index;
                    break;
                default:
                    break;
            }
        }

        if ($front_index != -1)
            array_push($sorted, $pages[$front_index]);

        if ($home_index != -1)
            array_push($sorted, $pages[$home_index]);

        $page_count = count($pages);

        foreach ($pages as $index => $page) {
            if ($index == $front_index || $index == $home_index)
                continue;

            array_push($sorted, $page);
        }

        return $sorted;
    }
}

if (!function_exists("wchtheme_get_attribution")) {
    /**
     * Return the attribution for a post
     * 
     * @param int $ID the ID of the post
     * @return string attribution for the post
     */
    function wchtheme_get_attribution(int $ID): string
    {
        return "Posted on ".get_post_time(get_option("date_format"), false, $ID);
    }
}

if (!function_exists("wchtheme_get_title")) {
    /**
     * Return the title for the page
     * 
     * @return string title string to display in the browser
     */
    function wchtheme_get_title(int $ID): string
    {
        $new_title = "";
        $page_on_front = get_option("page_on_front");

        if ($ID != $page_on_front) {
            $new_title = get_the_title($ID);

            $new_title = $new_title." - ";
        }
        
        $new_title = $new_title.get_bloginfo();
        
        return $new_title;
    }
}

if (!function_exists("wchtheme_setup")) {
    /**
     * Perform theme setup.
     *
     * @return void
     */
    function wchtheme_setup(): void
    {
        add_action("after_setup_theme", "wchtheme_support");
        add_action("wp_enqueue_scripts", "wchtheme_styles");
        add_action("wp_enqueue_scripts", "wchtheme_scripts");
        add_action("widgets_init", "wchtheme_widgets_init");
    }

    wchtheme_setup();
}
