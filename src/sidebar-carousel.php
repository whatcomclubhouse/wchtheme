<?php
/**
 * sidebar-carousel - a carousel widget for the theme
 * 
 * Copyright 2022 Whatcom Clubhouse
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
require_once ABSPATH."/wp-includes/formatting.php";
require_once ABSPATH."/wp-includes/media.php";

require_once __DIR__."/functions.php";

$attachments = wchtheme_get_carousel_attachments();

if (count($attachments)) {
?>
    <div id="wchtheme-bs-carousel" class="carousel slide text-center" data-bs-ride="carousel">
        <div class="carousel-indicators">
<?php
        foreach ($attachments as $index => $attachment) {
?>
            <button type="button" data-bs-target="#wchtheme-bs-carousel" data-bs-slide-to="<?php echo esc_attr($index); ?>" <?php echo $index == 0 ? "aria-current='true'" : ""; ?> class="<?php echo esc_attr($index == 0 ? "active" : ""); ?>" aria-label="Slide <?php echo $index + 1; ?>"></button> 
<?php
        }
?>
        </div>
        <div class="carousel-inner h-100">
<?php
        foreach ($attachments as $index => $attachment) {
            $image_src = wp_get_attachment_image_src($attachment->ID, "large");
?>
            <div class="carousel-item h-100 <?php echo esc_attr($index == 0 ? "active" : ""); ?>">
                <img class="h-100" src="<?php echo $image_src[0]; ?>" />
                <div class="carousel-caption">
                    <?php echo $attachment->post_excerpt; ?>
                </div>
            </div>
<?php
        }
?>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#wchtheme-bs-carousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#wchtheme-bs-carousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
<?php
}
