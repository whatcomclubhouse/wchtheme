<?php
/**
 * header - the header of the site
 * 
 * Copyright 2022 Whatcom Clubhouse
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
require_once ABSPATH."/wp-includes/formatting.php";
require_once ABSPATH."/wp-includes/general-template.php";
require_once ABSPATH."/wp-includes/link-template.php";
require_once ABSPATH."/wp-includes/post.php";
require_once ABSPATH."/wp-includes/post-template.php";
require_once ABSPATH."/wp-includes/query.php";

require_once ABSPATH."/wp-includes/class-wp-post.php";
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <title><?php echo wchtheme_get_title(get_queried_object_id()); ?></title>
        <meta charset="<?php bloginfo("charset"); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="manifest" src="<?php echo wchtheme_get_manifest(); ?>">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-success">
                <div class="container-fluid">
                    <a class="navbar-brand" href="<?php echo esc_url(home_url()); ?>">
<?php
                    if (has_site_icon()) {
?>
                        <img class="img-fluid" id="wchtheme-header-img" src="<?php site_icon_url(); ?>" alt="<?php bloginfo("name"); ?>" />
<?php
                    }
?>
                        <?php bloginfo("name"); ?>
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#wchtheme-navbar-content" aria-controls="wchtheme-navbar-content" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="wchtheme-navbar-content">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
<?php
                        $pages = get_pages();
                        $pages = wchtheme_nav_pages_sort($pages);

                        if ($pages) {
                            $queried_object_id = get_queried_object_id();
                            $page_on_front = get_option("page_on_front");
                            $page_for_posts = get_option("page_for_posts");
                            $blog_has_posts = wchtheme_has_regular_posts();

                            foreach ($pages as $page) {
                                if ($page->ID == $page_for_posts && !$blog_has_posts)
                                    continue;

                                $class_link = "nav-link";

                                if ($page->ID == $queried_object_id)
                                    $class_link = $class_link." active";
?>
                                <li class="nav-item">
                                    <a class="<?php echo esc_attr($class_link); ?>" aria-current="page" href="<?php echo esc_url(get_page_link($page->ID)); ?>"><?php echo esc_html($page->post_title); ?></a>
                                </li>
<?php
                            }
                        }
?>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <main class="container-fluid px-5 overflow-auto <?php echo esc_attr(is_admin_bar_showing() ? 'pb-5' : 'pb-3'); ?>" id="wchtheme-main">
