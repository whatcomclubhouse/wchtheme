<?php 
/**
 * front-page - the home page template
 * 
 * Copyright 2022 Whatcom Clubhouse
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
require_once ABSPATH."/wp-includes/author-template.php";
require_once ABSPATH."/wp-includes/general-template.php";
require_once ABSPATH."/wp-includes/link-template.php";
require_once ABSPATH."/wp-includes/post-template.php";
require_once ABSPATH."/wp-includes/option.php";
require_once ABSPATH."/wp-includes/query.php";

get_header();

get_sidebar("carousel");
if (is_active_sidebar("front_page_cards"))
    dynamic_sidebar("front_page_cards");

if (wchtheme_has_regular_posts()) {
?>
    <h2>News and Events:</h2>
    <div class="row">
<?php
        $regular_posts = wchtheme_get_regular_posts();

        foreach ($regular_posts as $regular_post) {
?>
            <div class="col-12 col-lg-4">
                <div <?php post_class(array("card", "my-2")); ?>>
                    <div class="card-header">
                        <div class="text-end">
                            <?php echo wchtheme_get_attribution($regular_post->ID); ?>
                        </div>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $regular_post->post_title; ?></h5>
                        <p class="card-text"><?php echo $regular_post->post_excerpt; ?></p>
                        <a href="<?php echo get_permalink($regular_post->ID); ?>" class="btn btn-sm btn-primary">See more</a>
                    </div>
                </div>
            </div>
<?php
        }
?>
    </div>
    <div class="text-center"><?php posts_nav_link(); ?></div>
<?php 
}

get_footer();
